const filehandeler = () => {

    const fs = require('fs');

    // Data to be written to the file
    const data = 'This is some sample data';

    // Path of the file to be created
    const filePath = './temp/test.txt';

    // Write the data to the file
    fs.writeFile(filePath, data, (err) => {
        if (err) throw err;
        console.log('Data written to file');

        // Delete the file
        fs.unlink(filePath, (err) => {
            if (err) throw err;
            console.log('File deleted');
        });
    });


}



