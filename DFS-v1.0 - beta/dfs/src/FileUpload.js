import React , { useState , useRef } from 'react'
import { create } from 'ipfs-http-client'
import  useEth  from "./contexts/EthContext/useEth"
import getfile from './security/encrpt'


const FileUpload = ({setUrl , setIpfsurl}) => {
  
  const [fileUrl, setFileUrl] = useState("");
  const ipfs = create('/ip4/127.0.0.1/tcp/5001')
  const [File, setFile] = useState({})
  const [loading, setLoading] = useState(false)
  const [uploaded, setUploaded] = useState(false)
  const { state } = useEth()
  const account = useRef()

  try{
    // console.log(account.current)
    account.current = state.accounts[0]    
  }
  
  catch{
    // console.log("wallet is not connected")
  }
  
  


  const uploadFile = async (e) => {
    setLoading(true)
    e.preventDefault()
    
    try{
      console.log("this where upload staet is called \n =====================================================")

      const added = await ipfs.add(File)
      console.log("this where done is called \n =====================================================")
      const urlipfs = `http://localhost:8080/ipfs/${added.path}`
      const url = `http://127.0.0.1:5001/ipfs/bafybeihcyruaeza7uyjd6ugicbcrqumejf6uf353e5etdkhotqffwtguva/#/ipfs/${added.path}`
      // console.log(url)

      setUrl(url)
      setFileUrl(url)
      // console.log(urlipfs)
      setIpfsurl(urlipfs)
      setUploaded(true)
    } 
    catch (err) {
      console.log('Error uploading the file : ', err)
    }
    finally{
      setLoading(false)
    }
  }




  const preupload =  (e) => {

    console.log("e starts here");
    
    // console.log(e.target.value);

    if (e.target.value !== '') {
      // setFile(e.target.files[0])
      const accnum = account.current
      const fileloc = e.target.files[0]
      // getfile(accnum,fileloc,{setFile})
      setFile(fileloc)
      
    } 
    else {
      setFile()
    }
    console.log(" fileloc")
    console.log("----------------------------------------------")
    console.log("ends here")

  }

  return (
    <div>
      
      <form onSubmit={uploadFile}>
        {/* File:{encryptedfile}<br/> */}

        <input type="file" onChange={(e) => preupload(e)} required></input>
        <input type="submit" value="Submit"></input>
      </form>

      
    </div>
  );
}

export default FileUpload;


