import React , { useState } from 'react';
import FileUpload from './Fileupload';
import { EthProvider } from "./contexts/EthContext";
import Demo from "./components/Demo";


const Application = () => {

  const [fileUrl,setFileUrl] = useState("");
  const [ipfsurl, setIpfsurl] = useState("")

  // console.log("app.js from src")
  // console.log(ipfsurl)

  
  return (
    <div>

      <EthProvider>
        <FileUpload setUrl={setFileUrl} setIpfsurl={setIpfsurl} />
        FileUrl : {""} <a href={fileUrl}>{fileUrl + ""}</a>
        <br /><br />
        IPFSurl : {""} <a href={ipfsurl} >{ipfsurl + ""}</a>

        <br></br>
        <div id="App">
          <div className="container">
            <h1>
            <Demo />
            </h1>
          </div>
        </div>
      </EthProvider>
    </div>
  );
};

export { Application };


