import React from 'react';
import * as FileSaver from 'file-saver';

function App() {
  const handleSaveImage = () => {
    fetch('http://bafybeiatam665s4hakrrhs7nabofga2j4ytfay6qyuee6rhz6yq2jpkluq.ipfs.localhost:8080/') // replace with your image URL
      .then(response => response.blob())
      .then(blob => {
        FileSaver.saveAs(blob, 'my-image.jpg'); // replace with your preferred filename
      });
  };

  return (
    <div>
      <button onClick={handleSaveImage}>Save Image</button>
    </div>
  );
}

export default App;
