import { Link, Route,Routes } from "react-router-dom";
import { Application } from "./Application"
import { Files } from "./Files"
import { Home } from "./Auth"
import  Decrypt  from "./Decrypt";
import Filehandeler from "./temp/Filehandeler";
import Tester from "./Tester";
// import { Encrpt } from "./Encrpt";


function App(){
  return <>
  
  <nav>
    <h1><br/>
      <Link to="/">Home</Link><br/>
      <Link to="/upload">Upload</Link><br/>
      <Link to="/files">Files</Link><br/>
      <Link to="/decrypt">decrypt</Link><br/>
      {/* <Link to="/filehandeler">Filehandeler</Link><br/> */}
      <Link to="/tester">Tester</Link><br/>
      <br/>

      {/* <Link to="/encrpt">Encrpt</Link> */}
    </h1>

  </nav>

  <Routes>
    <Route path='/' element={<Home />} />
    <Route path="/files" element={<Files />}/>
    <Route path="/upload" element={<Application />} />
    <Route path="/decrypt" element={<Decrypt />} />
    {/* <Route path="/filehandeler" element={<Filehandeler />} /> */}
    <Route path="/tester" element={<Tester />} />

    {/* <Route path="/encrpt" element={<Encrpt />} /> */}
  </Routes>
  </>
}

export default App