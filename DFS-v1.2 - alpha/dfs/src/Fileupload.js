import React , { useState , useRef, useEffect, useContext } from 'react'
import { create } from 'ipfs-http-client'
import  useEth  from "./contexts/EthContext/useEth"
import getfile from './security/encrpt'
import urlContext from './urlContext'
import Link from "./components/Demo/Ipfstochain"


const FileUpload = ({setUrl , setIpfsurl}) => {
  
  const [fileUrl, setFileUrl] = useState("");
  const ipfs = create('/ip4/127.0.0.1/tcp/5001')
  const [File, setFile] = useState({})
  const [loading, setLoading] = useState(false)
  const [uploaded, setUploaded] = useState(false)
  const { state } = useEth()
  const account = useRef()
  const [encrpt,setEncrpt] = useState(false)
  const accnum = account.current
  const {urlContextState,setUrlContextState} = useContext(urlContext)
  const [fileName,setFileName] = useState("nill")
  


  try{
    // console.log(account.current)
    account.current = state.accounts[0]    
  }
  
  catch{
    // console.log("wallet is not connected")
  }
  



  const uploadFile = async (e) => {
    setLoading(true)
    e.preventDefault()
    
    try{
      console.log("run")

      console.log("this where upload staet is called \n =====================================================")

      const added = await ipfs.add(File)
      console.log("this where done is called \n =====================================================")
      const urlipfs = `http://localhost:8080/ipfs/${added.path}`
      const url = `http://127.0.0.1:5001/ipfs/bafybeihcyruaeza7uyjd6ugicbcrqumejf6uf353e5etdkhotqffwtguva/#/ipfs/${added.path}`
      // console.log(url)
      // link = url
      // setLink(url)
      const storage = `<div><br/>${fileName}<br/>ipfs:<a href ="${urlipfs}">${urlipfs}</a><br/>url:<a href ="${url}">${url}</a><br/></div>`
      //url is created and context is modified pls do upload thiss in block chain
      setUrlContextState(storage)
      console.log("storage")
      console.log(storage)
      console.log("end storage")
      setUrl(url)
      setFileUrl(url)
      // console.log(urlipfs)
      setIpfsurl(urlipfs)
      setUploaded(true)

    } 
    catch (err) {
      console.log('Error uploading the file : ', err)
    }
    finally{
      setLoading(false)

      //adding the ipfs code here
      
    }
  }




  const preupload =  (e) => {

    console.log("e starts here");
    
    // console.log(e.target.value);

    if (e.target.value !== '') {
      // setFile(e.target.files[0])
      const fileloc = e.target.files[0]
      setFileName(e.target.files[0].name)
      if(encrpt){
      getfile(accnum,fileloc,{setFile})
      }
      else{
      setFile(fileloc)
      }
    } 
    else {
      setFile()
    }
    console.log(" fileloc")
    console.log("----------------------------------------------")
    console.log("ends here")

  }

  return (
    <div>
      
      <form onSubmit={(e) => {uploadFile(e)}}>

        {/* File:{encryptedfile}<br/> */}

        <input type="file" onChange={(e) => preupload(e)} required></input><br/>
        Encrypt<input type="checkbox" onChange={(e) => {setEncrpt(e.target.checked);console.log(e.target.checked)}}></input><br/>
        <input type="submit" value="Submit"></input><br/>
        <Link />
      </form><br/>

      
    </div>
  );
}

export default FileUpload;


