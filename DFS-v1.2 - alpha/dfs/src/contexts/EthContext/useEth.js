import { useContext } from "react";
import EthContext from "./EthContext";

const useEth = () => useContext(EthContext);
// console.log("use eth.js from ethcontext")


export default useEth;
