import { useState } from "react";
import useEth from "../../contexts/EthContext/useEth";
import { useRef, useEffect } from "react";
// console.log("index .js from demo componetets")


function ContractBtns({ setValue }) {
  const { state: { contract, accounts } } = useEth();
  const [inputValue, setInputValue] = useState("");
  // console.log("setvalue === " state.accounts)
  const handleInputChange = e => {

      setInputValue(e.target.value);

  };

  const read = async () => {
    const value = await contract.methods.read().call({ from: accounts[0] });
    console.log(value.join("\n"));
    setValue(value.join("\n<br>"));
  };

  const write = async e => {
    if (e.target.tagName === "INPUT") {
      return;
    }

    if (inputValue === "") {
      alert("Please enter a value to write.");
      return;
    }
    const newValue = (inputValue);
    await contract.methods.write(newValue).send({ from: accounts[0] });
  };

  return (
    <div className="btns">

      <button onClick={read}>
        read()
      </button>

      <div onClick={write} >
        <input
          type="text"
          placeholder="string"
          value={inputValue}
          onChange={handleInputChange}
        />done
      </div>

    </div>
  );
}

function NoticeNoArtifact() {
  return (
    <p>
      ⚠️ Cannot find <span className="code">SimpleStorage</span> contract artifact.
      Please complete the above preparation first, then restart the react dev server.
    </p>
  );
}

function NoticeWrongNetwork() {
  return (
    <p>
      ⚠️ MetaMask is not connected to the same network as the one you deployed to.
    </p>
  );
}

function Contract({ value }) {
  const spanEle = useRef(null);

  useEffect(() => {
    spanEle.current.classList.add("flash");
    const flash = setTimeout(() => {
      spanEle.current.classList.remove("flash");
    }, 300);
    return () => {
      clearTimeout(flash);
    };
  }, [value]);

  return (
    <code>

      <span className="secondary-color" ref={spanEle}>
        <strong dangerouslySetInnerHTML={{__html:value}}></strong>
      </span>
      
    </code>
  );
}

function Demo() {
  const { state } = useEth();
  const [value, setValue] = useState("?");

  const demo =
    <>
      <div className="contract-container">
        <Contract value={value} />
        
        <ContractBtns setValue={setValue} />
        
      </div>
    </>;

  // console.log(state.accounts[0])//the public key for encryption

  return (
    <div className="demo">
      {
        !state.artifact ? <NoticeNoArtifact /> :
          !state.contract ? <NoticeWrongNetwork /> :
            demo
      }
    </div>
  );
}

export default Demo;
