import React, { useState } from "react";
import CryptoJS from "crypto-js";
import * as FileSaver from 'file-saver';



function Filehandeler(data,m5key) {
    const reader = new FileReader();
    reader.onload = () => {
        console.log(reader.result);

        console.log("emotional damage "+CryptoJS.AES.decrypt(reader.result,m5key).toString(CryptoJS.enc.Utf8))
        FileSaver.saveAs(new Blob([CryptoJS.AES.decrypt(reader.result,m5key).toString(CryptoJS.enc.Utf8)]),"")
    }
    reader.readAsText(data);
}

export default Filehandeler;
