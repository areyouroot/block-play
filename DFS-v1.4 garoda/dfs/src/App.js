import { Link, Route,Routes } from "react-router-dom";
import { Application } from "./Application"
import { Files } from "./Files"
import { Home } from "./Auth"
import  Decrypt  from "./Decrypt";
import Tester from "./Tester";
// import { Encrpt } from "./Encrpt";


function App(){
  
  return <>
  <nav>
      <Link to="/"><img class="logo" src="https://ps.w.org/google-sitemap-plugin/assets/icon-256x256.gif?rev=2582517"/></Link>  
      <div class="headerRight">
      <Link to="/upload">Upload</Link>   
      <Link to="/files">Files</Link>  
      <Link to="/decrypt">decrypt</Link> 
      {/* <Link to="/filehandeler">Filehandeler</Link><br/> */}
      <Link to="/tester">Tester</Link>
      </div> 


      {/* <Link to="/encrpt">Encrpt</Link> */}

  </nav>

  <Routes>
    <Route path='/' element={<Home />} />
    <Route path="/files" element={<Files />}/>
    <Route path="/upload" element={<Application />} />
    <Route path="/decrypt" element={<Decrypt />} />
    {/* <Route path="/filehandeler" element={<Filehandeler />} /> */}
    <Route path="/tester" element={<Tester />} />

    {/* <Route path="/encrpt" element={<Encrpt />} /> */}
  </Routes>
  </>
}

export default App