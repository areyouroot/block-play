import { useContext, useState } from "react";
import useEth from "../../contexts/EthContext/useEth";
import { useRef, useEffect } from "react";
import urlContext from "../../urlContext";
// console.log("index .js from demo componetets")


function ContractBtns({urlContextState,setUrlContextState}) {

  const { state: { contract, accounts } } = useEth();
  const [inputValue, setInputValue] = useState("");
  console.log(`account - ${contract}`)
  console.log(`contract btns ${urlContextState}`)  
  


  const write = async (urlContextState) => {

    if (urlContextState == "") {
      alert("Please enter a value to write.");
      return;
    }
    const newValue = (urlContextState);
    await contract.methods.write(newValue).send({ from: accounts[0] });
  };

  write(urlContextState)

}

// function Contract({ value }) {
//   const spanEle = useRef(null);
//   useEffect(() => {
//     spanEle.current.classList.add("flash");
//     const flash = setTimeout(() => {
//       spanEle.current.classList.remove("flash");
//     }, 300);
//     return () => {
//       clearTimeout(flash);
//     };
//   }, [value]);
// }

function Link() {
  const { state } = useEth();
  const {urlContextState,setUrlContextState} = useContext(urlContext)
  console.log(`ipfs to chain 50 ${urlContextState}`)
  if (urlContextState != 'no URL'){
    ContractBtns({urlContextState,setUrlContextState})
  }
  else{
    console.log(`ipfs to chain ${urlContextState}`)
  }

}

export default Link;
