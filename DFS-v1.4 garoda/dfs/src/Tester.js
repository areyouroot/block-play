import React from 'react';

function FileByteViewer() {
  const handleFileUpload = (event) => {
    const file = event.target.files[0];

    const reader = new FileReader();

    reader.onload = (event) => {
      const contents = event.target.result;
      const bytes = new Uint8Array(contents);

      console.log(bytes);
    };

    reader.readAsArrayBuffer(file);
  };

  return (
    <div>
      <input type="file" onChange={handleFileUpload} />
    </div>
  );
}

export default FileByteViewer;
