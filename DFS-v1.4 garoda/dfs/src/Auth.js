export const Home = () => {
    return (
    <h1 class="home">
        Welcome to Decentralized File Storage System using <div>IPFS with Smart Contract</div>
        <a href="/upload"><img src="https://gitlab.com/areyouroot/block-play/-/raw/main/test%20upload/loading.gif"/>START UPLOADING</a>
    </h1>
    );
}