const crypto = require('crypto');
const fs = require('fs');

// Encryption function
function encrypt(inputFile, outputFile, password) {
  const readStream = fs.createReadStream(inputFile);
  const writeStream = fs.createWriteStream(outputFile);
  const cipher = crypto.createCipher('aes-256-cbc', password);

  readStream.pipe(cipher).pipe(writeStream);
}

// Decryption function
function decrypt(inputFile, outputFile, password) {
  const readStream = fs.createReadStream(inputFile);
  const writeStream = fs.createWriteStream(outputFile);
  const decipher = crypto.createDecipher('aes-256-cbc', password);

  readStream.pipe(decipher).pipe(writeStream);
}

// Example usage
const inputFile = 'path/to/input/file';
const encryptedFile = 'path/to/encrypted/file';
const decryptedFile = 'path/to/decrypted/file';
const password = 'mysecretpassword';

encrypt(inputFile, encryptedFile, password);
decrypt(encryptedFile, decryptedFile, password);