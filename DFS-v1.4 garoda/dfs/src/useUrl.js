import { useContext } from "react";
import urlContext from "./urlContext";

const useUrl = () => useContext(urlContext);
// console.log("use eth.js from ethcontext")


export default useUrl;
