// SPDX-License-Identifier: MIT

pragma solidity >=0.4.22 <0.9.0;

contract SimpleStorage {
  string[] public value;
  int public count=0;

  function read() public view returns (string[] memory) {
    return value;
  }

  function write(string memory newValue) public {

    value.push(newValue);
    count = count + 1;

  }
}







//default code


// pragma solidity >=0.4.22 <0.9.0;

// contract SimpleStorage {
//   string public value;

//   function read() public view returns (string memory) {
//     return value;
//   }

//   function write(string memory newValue) public {
//     value = newValue;
//   }
// }


